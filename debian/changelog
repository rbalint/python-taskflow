python-taskflow (3.7.1-3) unstable; urgency=medium

  * Removed Python 2 autopkg tests (Closes: #943182).

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 11:57:13 +0200

python-taskflow (3.7.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 10:29:32 +0200

python-taskflow (3.7.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Sep 2019 22:26:28 +0200

python-taskflow (3.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 14:48:31 +0200

python-taskflow (3.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Run unit tests using installed Python module.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 13:41:21 +0100

python-taskflow (3.2.0-3) unstable; urgency=medium

  * Add patch to fix StrEnum class that is breaking multiple packages at
    once (Closes: #910447).
  * Remove debhelper overrides.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Oct 2018 19:22:49 +0000

python-taskflow (3.2.0-2) unstable; urgency=medium

  * Fix autopkgtest depends (Closes: #908526).

 -- Thomas Goirand <zigo@debian.org>  Tue, 11 Sep 2018 09:12:05 +0200

python-taskflow (3.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Thomas Goirand ]
  * New upstream release.
  * Add reproducible_build.patch (Closes: #860374).
  * Removed fix-networkx-2-support.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Sep 2018 12:11:00 +0200

python-taskflow (3.1.0-5) unstable; urgency=medium

  * Fix description in fix-networkx-2-support.patch (Closes: #902985).

 -- Michal Arbet <michal.arbet@ultimum.io>  Sat, 28 Jul 2018 03:06:02 +0000

python-taskflow (3.1.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP

  [ Michal Arbet ]
  * d/control: Use pydot instead of pydotplus, because
    in sid we are using networkx > 2 which is using pydot
  * d/control: Add python3-openstackdocstheme to build-dependencies
  * d/control: Update Maintainer field
  * d/control: Update Uploaders field
  * d/control: Add python3-sphinx to build-dependencies
  * d/rules: Add deletion of .eggs
  * d/rules: Switch generating docs to python3-sphinx
  * d/patches: Add fix-networkx-2-support.patch
  * d/python-taskflow-doc.links: Fix lintian embedded-javascript-library

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Jul 2018 09:27:37 +0000

python-taskflow (3.1.0-3) unstable; urgency=medium

  * Blacklist test_to_dict_with_invalid_json_failures which is failing as a
    backport for Stretch, and which looks like relying on ordered dict, which
    is not really ordered.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2018 23:02:13 +0000

python-taskflow (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Blacklist test_examples.ExamplesTestCase.* which don't know how to properly
    find the taskflow module (Closes: #891438).

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:36 +0000

python-taskflow (3.1.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed XS-Testsuite: header.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Feb 2018 09:05:20 +0000

python-taskflow (2.14.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * Added missing Debian tests dependencies
  * Added Python module import Debian tests
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Sat, 07 Oct 2017 11:44:18 +0200

python-taskflow (1.30.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Added git as build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Mar 2016 10:24:58 +0100

python-taskflow (1.30.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Mar 2016 04:16:11 +0000

python-taskflow (1.26.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jan 2016 03:40:16 +0000

python-taskflow (1.25.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed use-async_utils.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Dec 2015 17:52:48 +0100

python-taskflow (1.21.0-3) unstable; urgency=medium

  * Downgrade python-networkx minimum version to 1.9 instead of 1.10, as it
    doesn't look like the update will come soon.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2015 23:20:16 +0000

python-taskflow (1.21.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 11:27:59 +0000

python-taskflow (1.21.0-1) experimental; urgency=medium

  [ Ivan Udovichenko ]
  * New upstream release.
  * d/control: Fixed (build-)depends for this release.
  * d/control: python-mysql is now replaced with python-pymysql.
  * d/control: unittest2 is not used anymore.
  * d/patches: Add use-async_utils.patch

  [ Thomas Goirand ]
  * Added python{3,}-netifaces as build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Sat, 03 Oct 2015 18:44:05 +0000

python-taskflow (1.17.0-3) experimental; urgency=medium

  * Added Python3 support (Closes: #798018).
  * Added a -doc package.
  * Fixed compat level.
  * Fixed wrong watch file URL (moved upstream).

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Sep 2015 07:56:16 +0000

python-taskflow (1.17.0-2) experimental; urgency=medium

  * Applied what's remaining as usefull in the Ubuntu patch (Closes: #791930).

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Sep 2015 13:49:30 +0200

python-taskflow (1.17.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Disabled unit test patch.
  * Black list a unit test:
    taskflow.tests.test_examples.ExamplesTestCase.test_99_bottles
    See LP bug: https://bugs.launchpad.net/taskflow/+bug/1479958

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 15:21:31 +0000

python-taskflow (1.15.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed build-depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Jul 2015 11:39:13 +0200

python-taskflow (0.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 10 Feb 2015 15:58:07 +0100

python-taskflow (0.4.0-2) experimental; urgency=medium

  * Added ordereddict in debian/pydist-overrides.

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Oct 2014 15:47:13 +0800

python-taskflow (0.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Updated (build-)depends for this release.
  * Do not support python 2.6 because of the need for ordereddict. Therefore
    setting-up X-Python-Version: >= 2.7.
  * Add patch to disable unit tests using Kombu 3.x, which isn't available
    normally with OpenStack Juno.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Oct 2014 16:31:11 +0800

python-taskflow (0.3.21-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Jul 2014 23:11:37 +0800

python-taskflow (0.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 08:04:17 +0000

python-taskflow (0.2-1) experimental; urgency=medium

  [ gustavo panizzo ]
  * New upstream release.
  * Increased the version required of python-six, python-iso8601,
    python-steveodore.

  [ Thomas Goirand ]
  * Some more (build-)depends fixes.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Apr 2014 08:35:03 +0000

python-taskflow (0.1.3-2) unstable; urgency=medium

  * Adds openstack-pkg-tools as build-depends. (Closes: #743070)
  * Standards-Version: is now 3.9.5
  * Using testrepository instead of nose.

 -- Thomas Goirand <zigo@debian.org>  Mon, 31 Mar 2014 11:15:22 +0800

python-taskflow (0.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Updated new upstream repository in d/copyright, d/rules and d/control.
  * Renamed python-futures into python-concurrent.futures for the
    (build-)depends (Closes: #739470).
  * Added python-subunit as build-depends.
  * Now needs python-testtools (>= 0.9.34)

 -- Thomas Goirand <zigo@debian.org>  Wed, 19 Feb 2014 15:23:19 +0800

python-taskflow (0.1.2-1) unstable; urgency=low

  * Initial release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Jan 2014 22:06:31 +0800
